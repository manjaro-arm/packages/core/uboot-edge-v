# U-Boot: Khadas Edge-V
# Maintainer: Furkan Kardame <furkan@fkardame.com>

pkgname=uboot-edge-v
pkgver=2021.10
pkgrel=1
_tfaver=2.5
pkgdesc="U-Boot for Khadas Edge-V"
arch=('aarch64')
url='http://www.denx.de/wiki/U-Boot/WebHome'
license=('GPL')
backup=('boot/extlinux/extlinux.conf')
makedepends=('bc' 'git')
install=${pkgname}.install

source=("ftp://ftp.denx.de/pub/u-boot/u-boot-${pkgver/rc/-rc}.tar.bz2"
        "https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/snapshot/trusted-firmware-a-$_tfaver.tar.gz"
        "0001-dts-rockchip-rk3399-enable-emmc-phy-for-spl.patch"
        "0001-fix-rk3399-suspend-correct-LPDDR4-resume-sequence.patch"
        "0002-fix-rockchip-rk3399-fix-dram-section-placement.patch")
sha256sums=('cde723e19262e646f2670d25e5ec4b1b368490de950d4e26275a988c36df0bd4'
            'ad8a2ffcbcd12d919723da07630fc0840c3c2fba7656d1462e45488e42995d7c'
            '54a6b35760f86023ad46e3febd5f97d32f0fcdba9b7775c531b452bcaf6d5eef'
            '2cd375c5456e914208eb1b36adb4e78ee529bdd847958fb518a9a1be5b078b12'
            '52c3641b59422cb4174ac5c0c1d8617917ac05472d0d0a3437db128c077673fb')

prepare() {
  cd u-boot-${pkgver/rc/-rc}
  # Patch based on the work of Yifeng
  patch -Np1 -i "${srcdir}/0001-dts-rockchip-rk3399-enable-emmc-phy-for-spl.patch"          # Missing eMMC PHY

  cd ../trusted-firmware-a-$_tfaver
  patch -Np1 -i "${srcdir}/0001-fix-rk3399-suspend-correct-LPDDR4-resume-sequence.patch"    # Suspend
  patch -Np1 -i "${srcdir}/0002-fix-rockchip-rk3399-fix-dram-section-placement.patch"       # GCC 11 fix
}

build() {
  # Avoid build warnings by editing a .config option in place instead of
  # appending an option to .config, if an option is already present
  update_config() {
    if ! grep -q "^$1=$2$" .config; then
      if grep -q "^# $1 is not set$" .config; then
        sed -i -e "s/^# $1 is not set$/$1=$2/g" .config
      elif grep -q "^$1=" .config; then
        sed -i -e "s/^$1=.*/$1=$2/g" .config
      else
        echo "$1=$2" >> .config
      fi
    fi
  }

  unset CFLAGS CXXFLAGS CPPFLAGS LDFLAGS

  cd trusted-firmware-a-$_tfaver

  echo -e "\nBuilding TF-A for Khadas Edge-V...\n"
  make PLAT=rk3399
  cp build/rk3399/release/bl31/bl31.elf ../u-boot-${pkgver/rc/-rc}

  cd ../u-boot-${pkgver/rc/-rc}

  echo -e "\nBuilding U-Boot for Khadas Edge-V...\n"
  make khadas-edge-v-rk3399_defconfig

  update_config 'CONFIG_IDENT_STRING' '" Manjaro Linux ARM"'
  update_config 'CONFIG_OF_LIBFDT_OVERLAY' 'y'
  update_config 'CONFIG_USB_EHCI_HCD' 'n'
  update_config 'CONFIG_USB_EHCI_GENERIC' 'n'
  update_config 'CONFIG_USB_XHCI_HCD' 'n'
  update_config 'CONFIG_USB_XHCI_DWC3' 'n'
  update_config 'CONFIG_USB_DWC3' 'n'
  update_config 'CONFIG_USB_DWC3_GENERIC' 'n'

  make EXTRAVERSION=-${pkgrel}
}

package() {
  cd u-boot-${pkgver/rc/-rc}

  mkdir -p "${pkgdir}/boot/extlinux"
  install -D -m 0644 idbloader.img u-boot.itb -t "${pkgdir}/boot"
} 
